import React, { useEffect, useState } from 'react'
import TicketsList from './tickets'

const Content = () => {
    const [tickets, setTickets] = useState([])
    const [filtered, setFiltered] = useState([])
    const [sorted, setSorted] = useState([])
    const [error, setError] = useState('')

    const [filters, setFilters] = useState([
        { name: "Все", checked: true },
        { name: "Без пересадки", checked: true, value: 0 },
        { name: "1 пересадка", checked: true, value: 1 },
        { name: "2 пересадки", checked: true, value: 2 },
        { name: "3 пересадки", checked: true, value: 3 },
    ])

    const [sort, setSort] = useState([
        { name: "Самый дешевый", checked: true, value: "asc" },
        { name: "Самый дорогой", checked: false, value: "desc" },
    ])

    //Get the tickets

    useEffect(() => {
        async function getTickets() {
            try {
                const searchIdResponce = await fetch("https://front-test.beta.aviasales.ru/search")
                const searchId = await searchIdResponce.json()
                const searchResponce = await fetch(`https://front-test.beta.aviasales.ru/tickets?searchId=${searchId.searchId}`)
                const data = await searchResponce.json()
                setTickets(data.tickets.slice(0,10))
            } catch (error) {
                setError(error.name)
            }
        }
        getTickets()
    }, [])


    //Listen for filters, and filter the tickets, after - new array with filtered tickets

    // useEffect(() => {
    //     let actualFilters = []
    //     filters.forEach(element => {
    //         if (element.checked === true) {
    //             actualFilters.push(element.value)
    //         }
    //     })

    //     setFiltered(Object.values(tickets).filter(data => actualFilters.indexOf(data.segments[0].stops.length) >= 0 && actualFilters.indexOf(data.segments[1].stops.length) >= 0))
    // }, [filters, tickets])

    useEffect(() => {
        const actualFilters = filters.filter(({ checked }) => checked).map(({ value }) => value);
        setFiltered(tickets.filter(({ segments }) => actualFilters.includes(segments[0].stops.length) && actualFilters.includes(segments[1].stops.length)))
    }, [filters, tickets])

    //Listen for sort type, and sort the tickets, after - new array with sorted tickets

    useEffect(() => {

        let newSorted = [...filtered]
        let actualSort = []
        sort.forEach(element => {
            if (element.checked === true) {
                actualSort.push(element.value)
            }
        })

        if (actualSort == "asc") {
            newSorted.sort((a, b) => Number(a.price) - Number(b.price))
        } else if (actualSort == "desc") {
            newSorted.sort((a, b) => Number(b.price) - Number(a.price))
        }


        setSorted(newSorted)
    }, [filtered, sort])

    //Set filters from UI

    function chooseFilters(element) {
        const newFilters = [...filters]

        if (element.target.id === "Все") {
            for (let i = 0; i < newFilters.length; i++) {
                let check = element.target.checked
                newFilters[0].checked = !check
                newFilters[i].checked = check
            }
        } else {
            newFilters[0].checked = false

        }

        let objIndex = newFilters.findIndex((obj => obj.name === element.target.id))
        newFilters[objIndex].checked = !newFilters[objIndex].checked
        setFilters(newFilters)
    }

    //Set sort type from UI

    function chooseSort(element) {
        const newSort = [...sort]

        for (let i = 0; i < newSort.length; i++) {
            newSort[i].checked = false
            let objIndex = newSort.findIndex((obj) => obj.value === element.target.id)
            newSort[objIndex].checked = true
        }
        setSort(newSort)
    }

    return (
        <div className="content">
            <div className="content_block">
                <div className="content_block_filter">
                    <p>Количество пересадок</p>
                    {filters.map((data, index) => (
                        <div key={index}>
                            <input
                                type="checkbox"
                                className="systemCheckbox"
                                id={data.name}
                                checked={data.checked}
                                onChange={e => chooseFilters(e)}
                            ></input>
                            <label htmlFor={data.name}>{data.name}</label>
                        </div>
                    ))}
                </div>
                <div className="content_block_tickets">
                    <div className="content_block_search">
                        {sort.map((data, index) => (
                            <>
                                <input
                                    type="checkbox"
                                    className="buttonCheckbox"
                                    id={data.value}
                                    checked={data.checked}
                                    onChange={e => chooseSort(e)}
                                ></input>
                                <label htmlFor={data.value}>{data.name}</label>
                            </>
                        ))}
                    </div>
                    <TicketsList data={sorted} error={error} />
                </div>
            </div>
        </div>
    )
}

export default Content