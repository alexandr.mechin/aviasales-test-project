import React from 'react'

const TicketsList = (element) => {

    if (element.data.length === 0) {
        if (element.error) {
            return(
                <div className="tickets">
                    <div className="loader-error"><p>Sorry. We found an {element.error} error. Please refresh your page. Thank you!</p></div>
                </div>
            )
        } else
        return(
        <div className="tickets">
            <div className="loader"><p>No tickets, according your filter</p></div>
        </div>
            )   
    }

    return (
        <>
            {element.data?.map((data, index) => (
                <div className="tickets" key={index}>
                    <div className="ticket_element">
                        <div className="tickets_first">
                            <div className="tickets_price">{new Intl.NumberFormat('ru-RU').format(data?.price)} P</div>

                            <img alt="" src={"//pics.avs.io/99/36/" + data?.carrier + ".png"}></img>
                        </div>
                        <div className="tickets_second">
                            <div className="tickets_grow">
                                <p className="text1">
                                    {data?.segments[0].origin} - {data?.segments[0].destination}
                                </p>
                            </div>
                            <div className="tickets_grow">
                                <p className="text1">
                                    В пути
                                </p>
                            </div>
                            <div className="tickets_grow">
                                <p className="text1">
                                    {data?.segments[0].stops.length} Пересадки
                                </p>
                            </div>
                        </div>
                        <div className="tickets_third">
                            <div className="tickets_grow">
                                <p className="text2">
                                    {(new Date(data?.segments[0].date)).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}{" - "}
                                    {new Date(new Date(data?.segments[0].date).getTime()+(data?.segments[0].duration*60*1000)).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}
                                </p>
                            </div>
                            <div className="tickets_grow">
                                <p className="text2">
                                    {parseInt(data?.segments[0].duration / 60) + "ч "}
                                    {data?.segments[0].duration - (parseInt(data?.segments[0].duration / 60) * 60)}м
                                </p>
                            </div>
                            <div className="tickets_stops tickets_grow align_right">
                                {data?.segments[0].stops.map((data, index) => (
                                    <p id={index} key={index} className="text2">{data}</p>
                                ))}
                            </div>
                        </div>
                    </div>
                    <div className="tickets_second">
                        <div className="tickets_grow">
                            <p className="text1">
                                {data?.segments[0].origin} - {data?.segments[0].destination}
                            </p>
                        </div>
                        <div className="tickets_grow">
                            <p className="text1">
                                В пути
                            </p>
                        </div>
                        <div className="tickets_grow">
                            <p className="text1">
                                {data?.segments[1].stops.length} Пересадки
                            </p>
                        </div>
                    </div>
                    <div className="tickets_third">
                        <div className="tickets_grow">
                            <p className="text2">
                                {(new Date(data?.segments[1].date)).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}{" - "}
                                {new Date(new Date(data?.segments[1].date).getTime()+(data?.segments[1].duration*60*1000)).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}
                            </p>
                        </div>
                        <div className="tickets_grow">
                            <p className="text2">
                                {parseInt(data?.segments[1].duration / 60) + "ч "}
                                {data?.segments[1].duration - (parseInt(data?.segments[1].duration / 60) * 60)}м
                            </p>
                        </div>
                        <div className="tickets_stops tickets_grow align_right">
                            {data?.segments[1].stops.map((data, index) => (
                                <p id={index} key={index} className="text2">{data}</p>
                            ))}
                        </div>
                    </div>
                </div>
            ))}
        </>
    )

}

export default TicketsList