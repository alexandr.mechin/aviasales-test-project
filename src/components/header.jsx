import React from 'react'
import mainLogo from '../assets/main_logo.svg'

const Header = () => {

    return (
    <div className="header">
        <a href="/">
            <img alt="" src={mainLogo}/>
        </a>
    </div>
    )
} 

export default Header